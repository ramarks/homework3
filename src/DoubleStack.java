import java.util.LinkedList;
import java.util.NoSuchElementException;

public class DoubleStack {

   private LinkedList<Double> dList;

   public static void main (String[] argum) {
	   DoubleStack ds = new DoubleStack();
	   System.out.println("Created new stack ds -> ds = " + ds);
	   System.out.println("ds.stEmpty(): " + ds.stEmpty());
	   ds.push(3.);
	   System.out.println("ds.push(3.) -> ds = " + ds);
	   ds.push(-5.2);
	   System.out.println("ds.push(-5.2) -> ds = " + ds);
	   System.out.println("ds.tos(): " + ds.tos() + " -> ds = " + ds);
	   System.out.println("ds.stEmpty(): " + ds.stEmpty());
	   System.out.println("ds.pop(): " + ds.pop() + " -> ds = " + ds);
	   DoubleStack ds2 = null;
	   try {
		   ds2 = (DoubleStack) ds.clone();
	   } catch (CloneNotSupportedException ex) {}
	   System.out.println("ds.clone() -> ds2 = " + ds2);
	   System.out.println("ds.equals(ds2): " + ds.equals(ds2));
	   System.out.println("ds == ds2: " + (ds == ds2));
	   ds.push(7.9);
	   System.out.println("ds.push(7.9) -> ds = " + ds);
	   ds.op("-");
	   System.out.println("ds.op(-) -> ds = " + ds);
       System.out.println("ds.interpret(\"-2.10 30.5 + 70 - = \") = " + 
			   	interpret("-2.10 30.5  + 70 -"));
	   System.out.println("ds.interpret(\"   \\t \\t356.  \\t \\t\") = " + 
			   	interpret("   \t \t356.  \t \t"));
	   try {
     	   interpret("-2.10 30.5  + + 70 -");		   
	   } catch (RuntimeException ex) {
		   System.err.println(ex);
	   }
	   try {
     	   interpret("-2.10 30.5  + 70 - 200");		   
	   } catch (RuntimeException ex) {
		   System.err.println(ex);
	   }
	   try {
     	   interpret("-2.10 30,5  + 70 - 200");		   
	   } catch (RuntimeException ex) {
		   System.err.println(ex);
	   }
   }

   DoubleStack() {
	   dList = new LinkedList<Double>();
   }

   @SuppressWarnings("unchecked")
   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack ds = new DoubleStack();
      ds.dList = (LinkedList<Double>) this.dList.clone();
	  return ds;
   }

   public boolean stEmpty() {
      return dList.size() == 0;
   }

   public void push (double a) {
	   dList.addLast(a);
   }

   public double pop() throws IllegalStateException {
	  try {
		  return dList.removeLast();		  
	  } catch (NoSuchElementException ex) {
		  throw new IllegalStateException("Stack is empty.");
	  }
   }

   public void op (String s) throws IllegalArgumentException {
	  if (s.length() > 1) {
		  throw new IllegalArgumentException(s + " is illegal operation.");
	  }
	  if (dList.size() < 2) {
		  throw new IllegalStateException("Not enough operands for the operator " + s + ".");
	  }
	  char cOp = s.charAt(0);
	  double d1, d2;
	  switch (cOp) {
	  case '+':
		  push(pop() + pop());
		  break;
	  case '-':
		  d1 = pop();
		  d2 = pop();
		  push(d2 - d1);
		  break;
	  case '*':
		  push(pop() * pop());
		  break;
	  case '/':
		  d1 = pop();
		  d2 = pop();
		  push(d2 / d1);
		  break;
	  default:
		  throw new IllegalArgumentException(cOp + " is illegal operation");
	  }
   }
  
   public double tos() throws IllegalStateException {
      try {
    	  return dList.getLast();    	  
      } catch (NoSuchElementException ex) {
    	  throw new IllegalStateException("Stack is empty.");
      }
   }

   @Override
   public boolean equals (Object o) {
      if ( (o != null) && (o instanceof DoubleStack) ) {
     	 return dList.equals( ((DoubleStack) o).dList );
      }
      return false;
   }

   @Override
   public String toString() {
	  return dList.toString();
   }

   public static double interpret (String pol) {
      DoubleStack ds = new DoubleStack(); 
	  String[] ops = pol.trim().split("\\s+");
      for (String oper: ops) {
    	  if ( oper.equals("+") || oper.equals("-") || 
    		   oper.equals("*") || oper.equals("/") ) {
    		  try {
    			  ds.op(oper);  
    		  } catch (IllegalStateException ise) {
    			  throw new IllegalArgumentException("\"" + pol + "\" is invalid expression. " + ise.getMessage());
    		  }
    		  
    	  } else {
    		  try {
    			  ds.push(Double.parseDouble(oper));
    		  } catch (NumberFormatException ex) {
    			  throw new NumberFormatException("Expression \"" + pol + 
    					  "\" contains illegal operand " + oper + ".");
    		  }
    	  }
      }
      double result = ds.pop();
      if ( !ds.stEmpty() ) {
    	  throw new IllegalArgumentException("\"" + pol + "\" is invalid expression. Too much operands");
      }
	  return result;
   }
}
